const { Role, User } = require('../../../../../app/models')
const { EmailNotRegisteredError, WrongPasswordError } = require('../../../../../app/errors');
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

describe('handleLogin function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    
    describe('Successful Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        
        it('(valid email and password) should return status code 201 and return accessToken of the corresponding logged in user', async () => {
            const userCredential = {
                email: "brian@binar.co.id",
                password: "123456"
            }
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeCalledWith(
                expect.objectContaining({
                    accessToken: expect.any(String),
                })
            )
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        
        it('(not registered email) should return status 404 and return error EmailNotRegisteredError', async () => {
            const userCredential = {
                email: "testing@binar.com",
                password: "123456"
            }
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            const expectedError = new EmailNotRegisteredError(mReq.body.email)
            expect(mRes.status).toBeCalledWith(404)
            expect(mRes.json).toBeCalledWith(expectedError)
        })

        it('(wrong password) should return status 401 and return error WrongPasswordError', async () => {
            const userCredential = {
                email: "brian@binar.co.id",
                password: "brian"
            }
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            const expectedError = new WrongPasswordError()
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expectedError)
        })

        it('(empty body) should hit handleError function', async () => {
            const mReq = { body: {  } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()

            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            expect(mNext).toHaveBeenCalled()
        })
    })
})

