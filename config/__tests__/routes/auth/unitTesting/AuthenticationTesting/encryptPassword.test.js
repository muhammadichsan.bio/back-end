const { Role, User } = require('../../../../../app/models')
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

describe('encryptPassword function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })
        it('(valid password) should return the hashed password', async () => {
            const resultHashedPassword = authenticationController.encryptPassword("123456")
            expect(resultHashedPassword).toBeDefined()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(blank password) should return the hashed password', async () => {
            const expectedError = new Error("Illegal arguments: undefined, string")
            const func = () => {
                try{
                    authenticationController.encryptPassword()
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })
    })
})
