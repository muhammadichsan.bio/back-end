const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('getCarFromRequest function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })

        it('(valid params query with page, pageSize, size and availableAt) should return list query object', async () => {
            const query = {
                size: "SMALL",
                availableAt: new Date(),
                page: 2, 
                pageSize: 15,

            }
            const resultQuery = carController.getListQueryFromRequest({ query })

            expect(resultQuery.include.where).toBeDefined()
            expect(resultQuery.where).toEqual({ size: query.size })
            expect(resultQuery.limit).toEqual((query.page - 1) * query.pageSize)
            expect(resultQuery.offset).toEqual(query.pageSize)
        })
        
    })

    describe('Error Operation', () => {

    })
})
