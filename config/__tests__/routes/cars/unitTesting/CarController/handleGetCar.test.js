const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('handleGetCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })
        it('(valid request params) should return status code 200 and return a car object', async () => {
            const mReq = { params: { id: 1 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleGetCar(mReq, mRes, mNext)


            const expectedResponse = await carModel.findOne({ where: mReq.params })
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })
    })

    describe('Error Operation', () => {

    })
})
