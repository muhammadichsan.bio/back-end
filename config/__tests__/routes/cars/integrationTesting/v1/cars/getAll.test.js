const app = require('../../../../../../app')
const request = require('supertest')

describe('Get All Route (GET /v1/cars)', () => {
    describe('Successful Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        it('(valid request query) should return status code 200 and with all cars and meta pagination', (done) => {
            const query = {
                page: 2,
                pageSize: 5,
                size: "SMALL",
                availableAt: new Date("2022-06-05 21:41:17.869+07")
            }
            request(app)
                .get("/v1/cars")
                .query(query)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(200)
                    expect(res.body.cars).toBeDefined()
                    expect(res.body.meta.pagination).toEqual(
                        expect.objectContaining({
                            page: expect.any(Number),
                            pageCount: expect.any(Number),
                            pageSize: expect.any(Number),
                            count: expect.any(Number)
                        })
                    )
                    done()

                })
        })
    })

    describe('Error Operation', () => {
        
    })
})