const app = require('../../../../../../app')
const { Car } = require('../../../../../../app/models')
const request = require('supertest')
const { JsonWebTokenError } = require('jsonwebtoken')
const { InsufficientAccessError } = require('../../../../../../app/errors')

describe('Create Route (POST /v1/cars)', () => {
    const carModel = Car

    describe('Successful Operation', () => {
        let token
        const userCredential = {
            email: "johnny@binar.co.id",
            password: "123456",
        }
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    done()
                })
            })

        afterAll(() => {

        })

        it('(valid request body) should be returning status code 201 and return the new creted car', (done) => {
            const newCar = {
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            }

            request(app)
                .post('/v1/cars')
                .set("Authorization", `Bearer ${token}`)
                .set('Accept', 'application/json')
                .send(newCar)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)

                    expect(res.body.name).toEqual(newCar.name)
                    expect(res.body.price).toEqual(newCar.price)
                    expect(res.body.size).toEqual(newCar.size)
                    expect(res.body.image).toEqual(newCar.image)
                    
                    await carModel.destroy({ where: { id: res.body.id } })
                    done()
            })
        })
    })
    describe('Error Operation', () => {
        let token
        const userCredential = {
            email: "brian@binar.co.id",
            password: "123456",
        }
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    done()
                })
            })

        afterAll(() => {

        })
        it('(invalid token) should be returning status code 401 and return error InsufficientAccessError', (done) => {
            const newCar = {
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            }
            const expectedError = new InsufficientAccessError("CUSTOMER")
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .post('/v1/cars')
                .set("Authorization", `Bearer ${token}`)
                .set('Accept', 'application/json')
                .send(newCar)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })
        it('(no authorization header) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const newCar = {
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            }
            const expectedError = new JsonWebTokenError("jwt must be provided")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .post('/v1/cars')
                .set('Accept', 'application/json')
                .send(newCar)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })
        it('(no token) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const newCar = {
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            }
            const expectedError = new JsonWebTokenError("jwt malformed")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .post('/v1/cars')
                .set('Authorization', `Bearer 123456`)
                .set('Accept', 'application/json')
                .send(newCar)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })
    })
})
