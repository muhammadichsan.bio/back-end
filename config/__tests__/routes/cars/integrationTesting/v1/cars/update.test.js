const app = require('../../../../../../app')
const { Car } = require('../../../../../../app/models')
const request = require('supertest')
const { JsonWebTokenError } = require('jsonwebtoken')
const { InsufficientAccessError } = require('../../../../../../app/errors')

describe('Update Route (PUT /v1/cars)', () => {
    const carModel = Car

    describe('Successful Operation', () => {
        let token
        const userCredential = {
            email: "johnny@binar.co.id",
            password: "123456",
        }
        let newCar
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    const newCarDetail = {
                        name: "Testing 1",
                        price: 1250000,
                        size: "SMALL",
                        image: "https://source.unsplash.com/531x531"
                    }
                    newCar = await carModel.create(newCarDetail)
                    done()
                })
            })

        afterAll(async () => {
            await carModel.destroy({ where: { id: newCar.id } }) 
        })

        it('(valid requst id and body) should be returning status code 201 and return the new creted car', (done) => {
            const updatedCarDetails = {
                name: "Testing 2",
                price: 50000000,
                size: "MEDIUM",
                image: "https://source.unsplash.com/500x500"
            }

            request(app)
                .put(`/v1/cars/${newCar.id}`)
                .set("Authorization", `Bearer ${token}`)
                .set('Accept', 'application/json')
                .send(updatedCarDetails)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(200)

                    expect(res.body.id).toEqual(newCar.id)
                    expect(res.body.name).toEqual(updatedCarDetails.name)
                    expect(res.body.price).toEqual(updatedCarDetails.price)
                    expect(res.body.size).toEqual(updatedCarDetails.size)
                    expect(res.body.image).toEqual(updatedCarDetails.image)
                    
                    await carModel.destroy({ where: { id: res.body.id } })
                    done()
            })
        })
    })
    describe('Error Operation', () => {
        let validToken
        const validUserCredential = {
            email: "johnny@binar.co.id",
            password: "123456"
        }
        
        let invalidToken
        const invalidUserCredential = {
            email: "brian@binar.co.id",
            password: "123456",
        }

        let newCar
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login').
                send(validUserCredential)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    validToken = res.body.accessToken

                    request(app)
                        .post('/v1/auth/login')
                        .send(invalidUserCredential)
                        .end(async (err, res) => {
                            if (err) return done(err)
        
                            expect(res.header['content-type']).toMatch(/json/)
                            expect(res.status).toEqual(201)
                            invalidToken = res.body.accessToken
                            
                            const newCarDetail = {
                                name: "Testing 1",
                                price: 1250000,
                                size: "SMALL",
                                image: "https://source.unsplash.com/531x531"
                            }
                            newCar = await carModel.create(newCarDetail)
                            done()
                        })
                })
            })

        afterAll(async () => {
            await carModel.destroy({ where: { id: newCar.id } })
        })

        it('(invalid request id but valid request body) should be returning status code 422 and return error of specific type', (done) => {
            const updatedCarDetails = {
                name: "Testing 2",
                price: 50000000,
                size: "MEDIUM",
                image: "https://source.unsplash.com/500x500"
            }
            const expectedError = new TypeError("Cannot read properties of null (reading 'update')")
            const expectedResponse = {
                error: {
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .put(`/v1/cars/-1`)
                .set("Authorization", `Bearer ${validToken}`)
                .set('Accept', 'application/json')
                .send(updatedCarDetails)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(422)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })

        it('(invalid token) should be returning status code 401 and return error InsufficientAccessError', (done) => {
            const updatedCarDetails = {
                name: "Testing 2",
                price: 50000000,
                size: "MEDIUM",
                image: "https://source.unsplash.com/500x500"
            }
            const expectedError = new InsufficientAccessError("CUSTOMER")
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .put(`/v1/cars/${newCar.id}`)
                .set("Authorization", `Bearer ${invalidToken}`)
                .set('Accept', 'application/json')
                .send(updatedCarDetails)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })

        it('(no authorization header) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const updatedCarDetails = {
                name: "Testing 2",
                price: 50000000,
                size: "MEDIUM",
                image: "https://source.unsplash.com/500x500"
            }
            const expectedError = new JsonWebTokenError("jwt must be provided")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .put(`/v1/cars/${newCar.id}`)
                .set('Accept', 'application/json')
                .send(updatedCarDetails)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })

        it('(no token) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const updatedCarDetails = {
                name: "Testing 2",
                price: 50000000,
                size: "MEDIUM",
                image: "https://source.unsplash.com/500x500"
            }
            const expectedError = new JsonWebTokenError("jwt malformed")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .put(`/v1/cars/${newCar.id}`)
                .set('Authorization', `Bearer 123456`)
                .set('Accept', 'application/json')
                .send(updatedCarDetails)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })
        
    })
})
