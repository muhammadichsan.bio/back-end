const app = require('../../../../../app')
const request = require('supertest')

describe('Index route (GET /)', () => {
    describe('Successful Operation', () => {
        beforeAll(() => { 
            
         })
         
        afterAll(() => { 
            
         })
        it('(valid request) should return status code 200 and message status OK and message BCR API is up and running!', (done) => {
            request(app)
                .get('/')
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(200)
                    expect(res.body.status).toEqual("OK")
                    expect(res.body.message).toEqual("BCR API is up and running!")
                    done()
                })
        })
    })

    describe('Error Operation', () => {
        
    })
})