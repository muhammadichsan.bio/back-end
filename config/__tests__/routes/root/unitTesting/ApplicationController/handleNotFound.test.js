const ApplicationController = require('../../../../../app/controllers/ApplicationController')
const { NotFoundError } = require('../../../../../app/errors')


describe('handleNotFound function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request) should return status code 404 and return error NotFoundError', async () => {
            const mReq = { method: "GET", url: "/hello" }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            applicationController.handleNotFound(mReq, mRes, mNext)

            const err = new NotFoundError(mReq.method, mReq.url)
            const expectedResult = {
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details
                }
            }
            expect(mRes.status).toBeCalledWith(404)
            expect(mRes.json).toBeCalledWith(expectedResult)
        })
    })

    describe('Error Operation', () => {
        
    })
})