const NotFoundError = require('../../app/errors/NotFoundError')

describe('NotFoundError Class', () => {
    describe('Successful create', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('should get details json object', async () => {
            const notFoundError = new NotFoundError("GET", "/hello")

            const expectedResult = {
                method: "GET",
                url: "/hello"
            }
            expect(notFoundError.details).toEqual(expectedResult)
        })
    })
    describe('Error create', () => {

    })
})