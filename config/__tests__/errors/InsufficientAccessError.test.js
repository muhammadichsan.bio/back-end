const InsufficientAccessError = require('../../app/errors/InsufficientAccessError')

describe('InsufficientAccessError Class', () => {
    describe('Successful create', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('should get details json object', async () => {
            const role = "ADMIN"
            const insufficientAccessError = new InsufficientAccessError(role)

            const expectedResult = {
                role,
                reason: `${role} is not allowed to perform this operation.`
            }
            expect(insufficientAccessError.details).toEqual(expectedResult)
        })
    })
    describe('Error create', () => {

    })
})