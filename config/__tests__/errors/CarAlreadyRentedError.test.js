const { CarAlreadyRentedError } = require('../../app/errors')
const { Car } = require('../../app/models')

describe('CarAlreadyRentedError Class', () => {
    const carModel = Car
    describe('Successful create', () => {
        let car
        beforeAll(async () => {
            car = await carModel.findByPk(10)

        })

        afterAll(() => {

        })

        it('should get details json object', async () => {
            const carAlreadyRentedError = new CarAlreadyRentedError(car)

            const expectedResult = {
                car: {
                    id: car.id,
                    image: car.image,
                    isCurrentlyRented: car.isCurrentlyRented,
                    name: car.name,
                    price: car.price,
                    size: car.size
                }
            }
            expect(carAlreadyRentedError.details).toEqual(expectedResult)
        })
    })
    describe('Error create', () => {
        
    })
})